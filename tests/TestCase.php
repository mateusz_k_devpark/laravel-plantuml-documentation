<?php

namespace Devpark\LaravelPlantumlDocumentation\Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use Orchestra\Testbench\TestCase as Orchestra;
use Devpark\LaravelPlantumlDocumentation\LaravelPlantumlDocumentationServiceProvider;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

        Factory::guessFactoryNamesUsing(
            fn (string $modelName) => 'Devpark\\LaravelPlantumlDocumentation\\Database\\Factories\\'.class_basename($modelName).'Factory'
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelPlantumlDocumentationServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        config()->set('database.default', 'testing');

        /*
        $migration = include __DIR__.'/../database/migrations/create_devpark-laravel-plantuml-documentation_table.php.stub';
        $migration->up();
        */
    }
}
